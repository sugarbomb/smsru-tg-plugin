package com.example.smsrutgplugin.MimickSmsruApi.controller

import com.example.smsrutgplugin.MimickSmsruApi.service.MimicService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class MimicController (
    val mimicService: MimicService
        ){

    @PostMapping("/sms/send")
    fun resendMessage(@RequestParam apiId: String?,
                    @RequestParam to: String,
                    @RequestParam msg: String,
                    @RequestParam json: String?) {
    mimicService.resendMessageToTg(apiId, to, msg, json)
    }
}