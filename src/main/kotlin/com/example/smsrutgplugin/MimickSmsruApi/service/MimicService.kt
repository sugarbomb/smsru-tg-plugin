package com.example.smsrutgplugin.MimickSmsruApi.service

import com.example.smsrutgplugin.telegram.TelegramSenderService
import org.springframework.stereotype.Service

@Service
class MimicService(
    val telegramSenderService: TelegramSenderService
) {

    fun resendMessageToTg(apiId: String?,
    to: String,
    msg: String,
    json: String?) {
        telegramSenderService.sendMessage(to, msg)
    }
}