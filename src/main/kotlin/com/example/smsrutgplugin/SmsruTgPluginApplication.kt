package com.example.smsrutgplugin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SmsruTgPluginApplication

fun main(args: Array<String>) {
    runApplication<SmsruTgPluginApplication>(*args)
}
