package com.example.smsrutgplugin.telegram

import com.example.smsrutgplugin.exceptions.MessageSendException
import com.example.smsrutgplugin.paths.SEND_MESSAGE_PATH
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import reactor.util.retry.Retry
import java.time.Duration

@Service
class TelegramSenderService
    (
    @Value("\${telegram.chatId}") val chatId: String,
    val telegramConfig: TelegramConfig
) {
    companion object : KLogging()

    @Throws(MessageSendException::class)
    fun sendMessage(phone: String, message: String) {

        val response: String = telegramConfig.telegramWebCLient()
            .post()
            .uri {uriBuilder ->
        uriBuilder.pathSegment(SEND_MESSAGE_PATH)
            .build()}
            .header("Content-Type", "application/json")
            .body(
                BodyInserters.fromValue(
                    mapOf(
                        "chat_id" to chatId,
                        "text" to String.format("%s\n%s", phone, message)
                    )
                )
            )
            .retrieve()
            .bodyToMono(String::class.java)
            .retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
            .block() ?: throw MessageSendException()

        logger.info("Response: $response")

    }

}