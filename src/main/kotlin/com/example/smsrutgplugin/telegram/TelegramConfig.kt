package com.example.smsrutgplugin.telegram

import com.example.smsrutgplugin.paths.BASE_TG_URL
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import java.lang.StringBuilder

@Configuration
class TelegramConfig (@Value ("\${telegram.bot-token}") val token: String){

    @Bean
fun telegramWebCLient(): WebClient =
WebClient.builder()
    .baseUrl(StringBuilder(BASE_TG_URL).append(token).toString())
    .build()
}